## Xenbase Small Molecule Ontology (XSMO) files 

### xsmo.obo
This is an obo file and contains pubchem and ChEBI xrefs, names, and synonyms for small molecules used in the curation of the Xenbase model organism database (http://www.xenbase.org/).

### xsmo.owl
This is an OWL formatted version of the ontology in 'xsmo.obo'.
